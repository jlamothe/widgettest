{-

Widget Test
Copyright (C) Jonathan Lamothe <jonathan@jlamothe.net>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

-}

module Main (main) where

import Brick.AttrMap (AttrMap, attrMap)
import Brick.Main (App (..), defaultMain, halt, showFirstCursor)
import Brick.Types (BrickEvent (VtyEvent), EventM, Widget)
import Brick.Util (on)
import Brick.Widgets.FileBrowser (
  FileBrowser,
  fileBrowserSelectedAttr,
  handleFileBrowserEvent,
  newFileBrowser,
  renderFileBrowser)
import Control.Monad (void)
import Graphics.Vty.Attributes.Color (black, white)
import Graphics.Vty.Input.Events (Event (EvKey), Key (KEsc))

app :: App (FileBrowser ()) () ()
app = App
  { appDraw         = draw
  , appChooseCursor = showFirstCursor
  , appHandleEvent  = eventHandler
  , appStartEvent   = return ()
  , appAttrMap      = const myAttrMap
  }

draw :: FileBrowser () -> [Widget ()]
draw = return . renderFileBrowser True

eventHandler :: BrickEvent () () -> EventM () (FileBrowser ()) ()
eventHandler (VtyEvent (EvKey KEsc _)) = halt
eventHandler (VtyEvent e) = handleFileBrowserEvent e
eventHandler _ = return ()

myAttrMap :: AttrMap
myAttrMap = attrMap (white `on` black)
  [ ( fileBrowserSelectedAttr , black `on` white )
  ]

main :: IO ()
main = void $ newFileBrowser (const True) () Nothing
  >>= defaultMain app

--jl
